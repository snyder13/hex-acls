'use strict';
// @flow

module.exports = ({ app }) => {
	const user = 'jill';

	app.get('/', (req, res) => {
		req.session.user = { 'id': 'jill' };
		res.render('index', { user });
	});

	app.get('/check', (req, res) => {
		app.locals.hasAcl(JSON.parse(req.query.spec))(req, res, () => {
			res.type('text').send('ok');
		});
	});
};
