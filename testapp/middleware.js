'use strict';
// @flow
import type { MiddlewareDefs } from 'hex/types';

const mw: MiddlewareDefs = module.exports = {
	'test': {
		'deps': [ 'hex-acls.checker', 'hex-acls.fs-store', 'hex.templates' ]
	}
};
