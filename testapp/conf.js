'use strict';
// @flow
module.exports = {
	'pg': {
		'user': 'hex_test',
		'password': 'hex_test',
		'database': 'hex_test'
	},
	'acls': {
		'file': `${__dirname}/acls.json`
	},
	'session': {
		'secret': 'not so secret'
	}
};
