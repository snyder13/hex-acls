'use strict';
// @flow

module.exports = {
	'up': ({ app }) => {
		return Promise.all([
			app.pg.query('INSERT INTO acls(user_id, domain, action, item_id, positive) VALUES ($1, $2, $3, $4, $5)', [ 'jill', 'domain', 'action', 'item', true ]),
			app.pg.query('INSERT INTO acls(user_id, domain, action, item_id, positive) VALUES ($1, $2, $3, $4, $5)', [ 'jane', 'domain', 'action', 'item', false ]),
			app.pg.query('INSERT INTO acls(domain, action, all_guests) VALUES ($1, $2, $3)', [ 'public', 'view', true ]),
			app.pg.query('INSERT INTO acls(domain, action, item_id, positive) VALUES ($1, $2, $3, $4)', [ 'public', 'view', 'specifically disallowed', false ]),
			app.pg.query('INSERT INTO acls(domain, action, all_registered) VALUES ($1, $2, $3)', [ 'registered', 'view', true ]),
			app.pg.query('INSERT INTO acls(domain, action, all_guests, positive) VALUES ($1, $2, $3, $4)', [ 'private', 'view', true, false ])
		]);
	},
	'down': ({ app }) => {
		return app.pg.query('TRUNCATE TABLE acls');
	}
};
