#!/usr/bin/env node
'use strict';

require('flow-remove-types/register')({ 'excludes': null });

const cwd = process.cwd(), mw = require(`${cwd}/middleware`), conf = require('hex/lib/conf')(cwd), log = require('hex/lib/log')(conf), prompt = require('prompt');

let store;
Object.keys(mw).some((k) => {
	if (mw[k].deps && mw[k].deps.some((dep) => {
		const ma = dep.match(/^hex-acls[.]([a-z]+-store)$/);
		if (ma) {
			store = ma[1];
			return true;
		}
	})) {
		return true;
	}
});

if (!store) {
	console.error('unable to determine acl storage from middleware.js in this path');
	process.exit();
}

console.log({ store });

const app = {
	'locals': {}
};
store = require(`${__dirname}/../middleware/${store}`)({ conf, app, log });

prompt.start();
prompt.get({ 'properties': {
	'domain': {
		'description': 'domain (general area of concern)',
		'type': 'string',
		'required': true
	},
	'action': {
		'description': 'action (verb applied to item, or the domain in general)',
		'type': 'string',
		'required': true
	},
	'item_id': {
		'description': 'item id, if any'
	},
	'user_id': {
		'description': 'user id, if any'
	},
	'group_id': {
		'description': 'group id, if any'
	},
	'all_registered': {
		'description': 'all registered users?',
		'type': 'boolean',
		'default': false
	},
	'positive': {
		'description': 'positive? true = allowed, false = disallowed by this acl',
		'type': 'boolean',
		'default': true
	}
} }, (err, res) => {
	if (err) throw err;
	Object.keys(res).forEach((k) => {
		if (res[k] === '') {
			res[k] = null;
		}
	});
	app.locals.aclStore.add(res).then(() => {
		console.log('ok');
	}, (err) => { throw err; });
});
