'use strict';
// @flow
import type { MiddlewareDefs } from 'hex/types';

const mw: MiddlewareDefs = module.exports = {
	'checker': {
		'description': 'access control list checker',
		'deps': [ 'hex.session' ],
		'after': [ 'pg-store', 'fs-store' ]
	},
	'pg-store': {
		'description': 'PostgreSQL storage for hex ACL',
		'deps': [ 'hex.pg' ]
	},
	'fs-store': {
		'description': 'Filesystem storage for hex ACL'
	}
};
