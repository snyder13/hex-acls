'use strict';
// @flow

module.exports = ({ app, log }) => {
	app.locals.aclStore = {
		'check': (spec, user) => {
			const where = [ 'all_guests' ];
			let params = [ spec.domain, spec.action ];
			if (spec.item_id) {
				params.push(spec.item_id);
				where.push('item_id = $3');
			}
			if (user.id) {
				where.push('all_registered')
				where.push(`user_id = $${params.length + 1}`);
				params.push(user.id);
				if (user.groups) {
					where.push(`group_id IN (${ Object.keys(user.groups).map((_v, k) => { return '$' + (k + params.length + 1); }).join(', ') })`);
					params = params.concat(Object.keys(user.groups));
				}
			}
			const sql = `SELECT positive
				FROM acls
				WHERE domain = $1 AND action = $2 AND (${where.join(' OR ')})
				ORDER BY CASE WHEN item_id IS NULL THEN 1 ELSE 0 END, CASE WHEN user_id IS NULL THEN 1 ELSE 0 END, CASE WHEN group_id IS NULL THEN 1 ELSE 0 END, CASE WHEN all_registered = true THEN 0 ELSE 1 END`;
			return app.pg.query(sql, params)
				.then((res) => {
//					log.debug({ sql, params, 'res': res.rows });
					return res.rows[0] && res.rows[0].positive;
				});
		}
	};
};
