'use strict';
// @flow

module.exports = ({ app, conf, log }) => {
	const login = conf.get('acls.login', null);
	app.locals.hasAcl = (spec, action, itemId, cb) => {
		if (typeof(spec) === 'string') {
			spec = { 'domain': spec };
		}
		if (isCallback(action)) {
			[ cb, action ] = [ action, null ];
		}
		else if (isCallback(itemId)) {
			[ cb, itemId ] =  [ itemId, null ];
		}
		else if (!isCallback(cb)) {
			cb = (req, res, next) => {
				if ((!req.user || !req.user.id) && login) {
					req.session.return = req.path;
					res.redirect(login);
				}
				else {
					next();
				}
			}
		}
		if (!spec.action) {
			spec.action = action;
		}
		if (!spec.item_id) {
			spec.item_id = itemId;
		}
		return (req, res, next) => {
			app.locals.aclStore.check(spec, req.user).then((allowed) => {
				if (allowed) {
					return next();
				}
				log.debug('acl failed', { spec, 'user': req.user });
				cb(req, res, () => {
					res.status(403).type('text').send(`Forbibben: ${JSON.stringify(spec)}`);
				});
			});
		};
	};
};

function isCallback(obj) {
	return obj && obj.constructor && obj.constructor === Function;
}
