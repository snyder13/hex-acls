'use strict';
// @flow

const fs = require('fs'), defaults = {
	'domain': undefined,
	'action': undefined,
	'group_id': null,
	'item_id': null,
	'user_id': null,
	'all_registered': false,
	'positive': true
};

module.exports = ({ app, log, conf }) => {
	const file = conf.get('acls.file');
	if (!fs.existsSync(file)) {
		fs.writeFileSync(file, '[]');
	}
	const perms = {}, addPerm = (spec) => {
		const domAct = `${spec.domain}.${spec.action}`;
		if (!perms[domAct]) {
			perms[domAct] = [];
		}
		perms[domAct].push(spec);
	}
	const raw = JSON.parse(fs.readFileSync(file, { 'encoding': 'utf8' }));
	raw.forEach(addPerm);
	const sort = (a, b) => {
		const specs = [ 'item_id', 'user_id', 'group_id' ];
		for (let idx = 0; idx < specs.length; ++idx) {
			if (a[specs[idx]] && !b[specs[idx]]) {
				return -1;
			}
			if (b[specs[idx]] && !a[specs[idx]]) {
				return 1;
			}
		}
		if (a.all_registered && !b.all_registered) {
			return -1;
		}
		if (b.all_registered && !a.all_registered) {
			return 1;
		}
		return 0;
	};
	Object.keys(perms).forEach((k) => {
		perms[k].sort(sort);
	});

	app.locals.aclStore = {
		'check': (spec, user) => {
			if (!user) {
				user = {};
			}
			return new Promise((resolve, reject) => {
				const domAct = `${spec.domain}.${spec.action}`;
				if (!perms[domAct]) {
					return resolve(false);
				}
				if (!perms[domAct].some((perm) => {
					if (perm.item_id && perm.item_id !== spec.item_id) {
						return false;
					}
					if (user.id) {
						if (user.id === perm.user_id) {
							resolve(perm.positive);
							return true;
						}
						if (user.groups && perm.group_id && user.groups.includes(perm.group_id)) {
							resolve(perm.positive);
							return true;
						}
						if (perm.all_registered) {
							resolve(perm.positive);
							return true;
						}
					}
					if (!perm.user_id) {
						resolve(perm.positive);
						return true;
					}
				})) {
					resolve(false);
				}
			});
		},
		'add': (spec) => {
			return new Promise((resolve, reject) => {
				if (Object.keys(defaults).some((k) => {
					if (spec[k] === undefined) {
						if (defaults[k] === undefined) {
							reject(`missing required attribute ${k}`);
							return true;
						}
						spec[k] = defaults[k];
					}
				})) {
					return;
				}
				addPerm(spec);
				perms[`${spec.domain}.${spec.action}`].sort(sort);
				raw.push(spec);
				fs.writeFile(file, JSON.stringify(raw), (err, res) => {
					if (err) return reject(err);
					resolve();
				})
			});
		}
	};
};
