'use strict';
// @flow

module.exports = {
	'up': ({ app }) => {
		return app.pg.query(
`CREATE TABLE acls(
	id serial primary key,
	user_id varchar(100) null,
	all_registered boolean default false,
	domain varchar(100) NOT NULL,
	action varchar(50) null,
	item_id varchar(100) null,
	positive boolean not null default true,
	all_guests boolean not null default false,
	group_id varchar(100) null)`
		);
	},
	'down': ({ app }) => {
		return app.pg.query('DROP TABLE acls');
	}
};
